﻿using UnityEngine;
using System.Collections;

public class LerpControl : MonoBehaviour {

    public centreRing cr;
    public middleRing mr;
    public OutsideRing or;

    public bool centreLerp;
    public bool middleLerp;
    public bool outsideLerp;
    public bool allOfTheAbove;


	void Update () {

        //centre ring
        if (centreLerp)
        {
            cr.lerp = true;
        }
        else if (!centreLerp)
        {
            cr.lerp = false;
        }

        //middle ring
        if (middleLerp)
        {
            mr.lerp = true;
        }
        else if (!middleLerp)
        {
            mr.lerp = false;
        }

        //outside ring
        if (outsideLerp)
        {
            or.lerp = true;
        }
        else if (!outsideLerp)
        {
            or.lerp = false;
        }

        //all rings
        if (allOfTheAbove)
        {
            cr.lerp = true;
            mr.lerp = true;
            or.lerp = true;
        }
	}
}
