﻿using UnityEngine;
using System.Collections;

public class RotateZUpdate : MonoBehaviour {

    public GameObject refGo;
    public GameObject head;
    
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        Quaternion rot = Quaternion.LookRotation(refGo.transform.forward, head.transform.forward); //declares a rotation that looks in the direction of -gravity while it's up axis faces the camera's forward vector
        transform.rotation = rot;

        //if (Vector3.Angle(transform.right, transform.GetChild(1).transform.right) >= 10f){
        //    transform.rotation = Quaternion.FromToRotation(gameObject.transform.right, transform.GetChild(1).transform.right);
        //}
    }
}
