﻿using UnityEngine;
using System.Collections;

public class QuartTranslation : MonoBehaviour {

    public GameObject refGo;
    public Vector3 targetQ;

    public float xRot;
    public float yRot;
    public float zRot;

	// Update is called once per frame
	void Update () {
        targetQ = refGo.transform.rotation.eulerAngles;
        xRot = refGo.transform.rotation.eulerAngles.x;
        yRot = refGo.transform.rotation.eulerAngles.y;
        zRot = refGo.transform.rotation.eulerAngles.z;
    }
}
