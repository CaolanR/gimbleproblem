﻿using UnityEngine;
using System.Collections;

public class DirTest : MonoBehaviour {

    public GameObject head;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        //print("rot in world space" + gameObject.transform.TransformDirection(gameObject.transform.position));
        //print("gravity in world space" + transform.TransformDirection(gameObject.transform.position));
        //transform.LookAt(-Physics.gravity);
        Quaternion rot = Quaternion.LookRotation(-Physics.gravity, head.transform.forward); //declares a rotation that looks in the direction of -gravity while it's up axis faces the camera's forward vector

        transform.rotation = rot;
        //print("normalized grav = " + Physics.gravity.normalized);

    }
}
