﻿using UnityEngine;
using System.Collections;

public class middleRing : MonoBehaviour {

    public float zAngle;
    public float lerpPos;
    public float lerpSpeed;

    public GameObject refRing;

    public bool lerp = false;
    public bool anglePos;
    public bool correctRot;

    public Vector3 xRot;
    public Vector3 xCurrentRot;

    void Update()
    {
        zAngle = Vector3.Angle(gameObject.transform.right, refRing.transform.right);

        if (!correctRot)
        {
            if (lerp)
            {
                lerpPos += Time.deltaTime * lerpSpeed;

                if (anglePos)
                {
                    gameObject.transform.Rotate(0, 0, Mathf.Lerp(0, -zAngle, lerpPos));
                }

                else if (!anglePos)
                {
                    gameObject.transform.Rotate(0, 0, Mathf.Lerp(0, zAngle, lerpPos));
                }
            }
        }

        if (Vector3.Dot(gameObject.transform.up, -refRing.transform.right) > 0)
        {
            anglePos = true;
            correctRot = false;
        }

        else if (Vector3.Dot(gameObject.transform.up, -refRing.transform.right) < 0)
        {
            anglePos = false;
            correctRot = false;
        }

        if (zAngle == 0)
        {
            correctRot = true;
            lerp = false;
            lerpPos = 0f;
        }

        else if (zAngle != 0)
        {
            correctRot = false;
        }
    }
}

